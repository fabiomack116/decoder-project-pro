package com.ead.authuser.models;

import com.ead.authuser.enums.UserStatus;
import com.ead.authuser.enums.UserType;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

@Data // Através da dependência lumbok e instanciando o @Data nos permitirá criar getter e setter automaticmante para cada coluna
@JsonInclude(JsonInclude.Include.NON_NULL) // Jackson - Ao serializar, ele vai ignorar todos valores das colunas que estiverem vazias.
@Entity
@Table(name = "TB_USERS")
// o Serializable converte a classe em um conjunto de bytes para ser inserido no banco de dados
public class UserModel extends RepresentationModel<UserModel> implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO) // Será gerado um id de forma automatica, porém utilizando UUID
    private UUID userId;

    @Column(nullable = false, unique = true, length = 50) // não pode ser nulo e deve ser unico e só pode ter 50 caracteres
    private String username;

    @Column(nullable = false, unique = true, length = 50)
    private String email;

    @Column(nullable = false, length = 255)
    @JsonIgnore // Não trazer password no JSON quando realizar requisição, pois é um dado privado
    private String password;

    @Column(nullable = false, length = 150)
    private String fullName;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING) // Fará um parse no valor para salvar como string no banco de dados
    private UserStatus userStatus;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private UserType userType;

    @Column(length = 20)
    private String phoneNumber;

    @Column(length = 20)
    private String cpf;

    @Column
    private String imageUrl;

    @Column(nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime creationDate;

    @Column(nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    private LocalDateTime lastUpdateDate;

}
